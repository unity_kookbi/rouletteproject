using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class RouletteObject : MonoBehaviour
{
    [Header("룰렛 게임 객체")]
    public RouletteGame m_RouletteGame;

    [Header("아이템 문자열")]
    public List<TMP_Text> m_ItemTexts;

    [Header("룰렛 오브젝트")]
    public GameObject m_RouletteObject;

    [Header("결과 표시용 텍스트")]
    public TMP_Text m_ResultText;

    [Header("다시하기 버튼")]
    public Button m_RestartButton;

    /// <summary>
    /// z 축 회전 속도입니다.
    /// 0일 경우 회전하지 않습니다.
    /// </summary>
    private float _ZRotationVelocity;

    /// <summary>
    /// 제동력입니다.
    /// 해당 수치만큼 매 프레임마다 회전 속도를 감소시킵니다.
    /// </summary>
    private float _BreakingForce = 1500.0f;

    /// <summary>
    /// 룰렛 오브젝트 회전을 허용시키기 위한 변수
    /// </summary>
    private bool _IsRotatingAllowed;

    private void Awake()
    {
        // 결과 텍스트 비활성화
        m_ResultText.gameObject.SetActive(false);

        // 다시하기 버튼 비활성화
        m_RestartButton.gameObject.SetActive(false);

        // 버튼 이벤트 설정
        m_RestartButton.onClick.AddListener(OnRestartButtonClicked);

    }

    private void Update()
    {
        // 룰렛을 회전시킵니다.
        RotationRoulette();

        // 결과를 출력합니다.
        ShowResult();
    }

    /// <summary>
    /// 회전이 중단되는 경우 호출될 매서드입니다.
    /// </summary>
    private void OnRotationFinished()
    {
        m_RestartButton.gameObject.SetActive(true);

        m_ResultText.gameObject.SetActive(true);

    }

    /// <summary>
    /// 결과를 표시합니다.
    /// </summary>
    private void ShowResult()
    {
        // 핀과 가장 가까운 아이템을 나타내기 위한 변수
        TMP_Text nearestText = null;

        // 핀과 가장 가까운 아이템의 각도를 저장하기 위한 변수
        float nearestAngle = 360.0f;

        foreach (TMP_Text textComponent in m_ItemTexts)
        {
            float angle = Vector2.SignedAngle(textComponent.transform.up, Vector2.up);

            angle = Mathf.Abs(angle);

            // 더 작은 각을 이루는 요소를 찾음
            if(nearestAngle > angle)
            {
                // 각도를 저장
                nearestAngle = angle;

                // 텍스트 컴포넌트를 저장합니다.
                nearestText = textComponent;
            }
        }

        // 결과 표시
        m_ResultText.text = "결과는 : " + nearestText.text;
    }

    /// <summary>
    /// 룰렛을 회전시킵니다.
    /// </summary>
    private void RotationRoulette()
    {
        // 회전이 허용되지 않았을 경우 메서드 호출을 중단합니다.
        if (!_IsRotatingAllowed) return;

        // 룰렛의 현재 회전값을 얻어 다음 회전을 설정합니다.
        Vector3 rouletteRotation = m_RouletteObject.transform.localEulerAngles;

        // 회전 속도에 제동력을 적용합니다.
        _ZRotationVelocity -= _BreakingForce * Time.deltaTime;

        // 회전 속도가 0 미만인 경우 회전을 진행하지 않습니다.
        if(_ZRotationVelocity < 0)
        {
            // 회전 비허용
            _IsRotatingAllowed = false;

            // 회전 멈춤
            OnRotationFinished();

            return;
        }

        // 다음 회전값을 설정합니다.
        rouletteRotation.z -= _ZRotationVelocity * Time.deltaTime;

        // 회전값 적용
        m_RouletteObject.transform.localEulerAngles = rouletteRotation;
    }

    /// <summary>
    /// 회전 속도를 초기화 합니다.
    /// </summary>
    private void InitializeRotationVelocity()
    {
        _ZRotationVelocity = Random.Range(2000.0f, 3000.0f);
    }

    /// <summary>
    /// 룰렛 오브젝트를 초기화합니다.
    /// </summary>
    /// <param name="itemStrings">설정시킬 문자열들을 전달합니다.</param>
    public void InitializeRoullete(string[] itemStrings)
    {
        // 아이템 문자열을 모두 설정합니다.
        for(int i = 0; i < m_ItemTexts.Count; i++) 
        {
            // 텍스트 컴포넌트를 배열에서 얻습니다.
            TMP_Text textComponent = m_ItemTexts[i];

            // 설정시킬 문자열을 배열에서 얻습니다.
            string itemString = itemStrings[i];

            // 텍스트 컴포넌트의 문자열을 설정합니다.
            textComponent.text = itemString;
        }

        // 회전 속도 초기화
        InitializeRotationVelocity();

        // 회전 허용
        _IsRotatingAllowed = true;
    }


    /// <summary>
    /// 다시하기 버튼이 클릭된 경우 호출됩니다.
    /// </summary>
    private void OnRestartButtonClicked()
    {
        // 다시하기 버튼 비활성화
        m_RestartButton.gameObject.SetActive(false);

        // 결과 창 비활성화
        m_ResultText.gameObject.SetActive(false);

        // 세팅 패널 활성화
        m_RouletteGame.OnGameRestarted();

        // 회전을 초기화합니다.
        m_RouletteObject.transform.eulerAngles = Vector3.zero;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        // 기즈모 색상을 설정합니다.
        Gizmos.color = Color.red;

        // 그릴 때 사용될 좌표행렬 정보를 얻습니다.
        Matrix4x4 screenMatrix = (transform.parent.transform as RectTransform).localToWorldMatrix;
        // transform.parent : 이 컴포넌트를 소유하는 오브젝트의 부모 오브젝트

        // 기즈모가 그려질 좌표행렬 정보를 설정합니다.
        Gizmos.matrix = screenMatrix;
        Handles.matrix = screenMatrix;

        // 룰렛 오브젝트의 RectTransform
        RectTransform rouletteTransform = m_RouletteObject.transform as RectTransform;

        // 시작점을 설정합니다.
        Vector2 drawLineStart = rouletteTransform.anchoredPosition;

        foreach (TMP_Text textComponent in m_ItemTexts) 
        {
            // 끝점을 설정합니다.
            Vector2 drawLineEnd = textComponent.rectTransform.up * 280.0f;

            // 선을 그립니다.
            Gizmos.DrawLine(drawLineStart, drawLineEnd);

            // 룰렛의 중앙부터 핀까지의 선분과
            // 룰렛의 중앙부터 아이템 텍스트 위치까지의 선분 사이 각도를 구합니다.
            float angle = Vector2.SignedAngle(textComponent.transform.up, Vector2.up);
            // SignedAngle(from, to) : from 과 to 벡터 사이의 각도를 구합니다.

            //Vector3 offset = textComponent.transform.up - Vector3.up;
            //float angle = Mathf.Atan(offset.y / offset.x) * Mathf.Rad2Deg * 2;

            angle = Mathf.Abs(angle);

            Handles.Label(drawLineEnd, angle.ToString());
        }

    }

#endif
}
